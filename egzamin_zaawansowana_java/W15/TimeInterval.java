import java.time.DayOfWeek;
import java.time.LocalTime;

public class TimeInterval {
    private LocalTime startTime;
    private LocalTime endTime;
    private DayOfWeek dayOfWeek;
    public TimeInterval() {};
    public TimeInterval(LocalTime start, LocalTime end, DayOfWeek day) throws Exception {
        if (start.isAfter(end))
            throw new Exception("Błędny interwał: czas początkowy jest większy niż czas końcowy.");
        startTime = start;
        endTime = end;
        dayOfWeek = day;
    }

    public LocalTime getStartTime() { return startTime; }
    public LocalTime getEndTime() { return endTime; }
    public DayOfWeek getDayOfWeek() { return dayOfWeek; }

    public boolean overlapsOver(TimeInterval ti) {
        return ti.getDayOfWeek() == dayOfWeek
            && (
                (ti.getStartTime().isAfter(startTime) && ti.getEndTime().isBefore(endTime)) ||
                (ti.getStartTime() == startTime && ti.getEndTime() == endTime)
            );
    }
}
