import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Locale;
import java.util.Scanner;
import java.util.function.Predicate;

class W15 {
    private static void separator(Scanner reader) {
        System.out.println();
        try {
            reader.nextLine();
        } catch (Exception e) {}
        System.out.println();
    }

    private static TemporalAdjuster nastepny(Predicate<LocalDate> predict) {
        return TemporalAdjusters.ofDateAdjuster(w -> {
            LocalDate result = w;
            do {
                result = result.plusDays(1);
            } while (!predict.test(result));
            return result;
        });
    }

    private static LocalTime calculatePeriodOffset(LocalTime localTime, LocalTime offsetTime, ZoneId source, ZoneId target) {
        ZonedDateTime sourceTime = ZonedDateTime.of(LocalDate.now(source), localTime, source);
        sourceTime = sourceTime.plusSeconds((offsetTime.getHour()*3600) + (offsetTime.getMinute()*60) + offsetTime.getSecond());
        return ZonedDateTime.of(sourceTime.toLocalDate(), sourceTime.toLocalTime(), target).toLocalTime();
    }

    
    private static int calculateTravelTime(LocalTime sourceTime, ZoneId source, LocalTime targetTime, ZoneId target) {
        ZonedDateTime beginTime = ZonedDateTime.of(LocalDate.now(source), sourceTime, source);
        ZonedDateTime endTime = ZonedDateTime.of(LocalDate.now(target), targetTime, target);
        return (int) ChronoUnit.MINUTES.between(beginTime, endTime);
    }

    public static void main(String[] args) throws Exception {
        System.out.println("Zadania są podzielone klawiszem ENTER, oprócz zadania 5.");
        Scanner reader = new Scanner(System.in);

        separator(reader);
        {
            LocalDate programistsDay = LocalDate.ofYearDay(LocalDate.now().getYear(), 256);
            System.out.println(String.format("Z1. Dzień programisty jest %s-ego %s tego roku (%s).",
                    programistsDay.getDayOfMonth(),
                    programistsDay.getMonth().getDisplayName(TextStyle.FULL, new Locale("pl", "PL")),
                    programistsDay.getYear()));
        }

        separator(reader);

        {
            System.out.println("Z2. Do daty 29 lutego 2000 dodaję rok,");
            System.out.print("    a po dodaniu dostaję: ");
            System.out.println(LocalDate.of(2000, 2, 29).plusYears(1).toString());
            System.out.println("    Z roku przestępnego 2000 przeskakuje na");
            System.out.println("    rok zwykły, i luty ma 28 dni w 2001 roku.");
        }

        separator(reader);

        {
            LocalDate nextWorkDay = LocalDate.now().with(nastepny(w -> w.getDayOfWeek().getValue() < 6));
            System.out.println();
            System.out.println(String.format(
                    "Z3. Następnym dniem roboczym od dzisiaj (" + LocalDate.now().toString()
                            + ") będzie %s-ego %s tego roku (%s).",
                    nextWorkDay.getDayOfMonth(),
                    nextWorkDay.getMonth().getDisplayName(TextStyle.FULL, new Locale("pl", "PL")),
                    nextWorkDay.getYear()));
        }

        separator(reader);

        {
            System.out.println("Z4. Wykonanie (java) Cal 3 2013 daje nam wynik:");
            Cal.main(new String[] { "3", "2013" });
        }

        separator(reader);

        {
            System.out.println("Z5. Obliczanie ile dni żyjesz!");
            System.out.println("Podaj dzień Twoich narodzin:");
            String daystr = reader.next();
            System.out.println("Podaj miesiąc Twoich narodzin:");
            String monthstr = reader.next();
            System.out.println("Podaj rok Twoich narodzin:");
            String yearstr = reader.next();

            Year year;
            YearMonth month;
            LocalDate day;

            try {
                year = Year.of(Integer.parseInt(yearstr));
                month = year.atMonth(Integer.parseInt(monthstr));
                day = month.atDay(Integer.parseInt(daystr));
            } catch (Exception e) {
                System.out.println("Podano nieprawidłową datę narodzin.");
                return;
            }

            System.out.println(day);
            int daysAlive = (int) ChronoUnit.DAYS.between(day, LocalDate.now());
            /*LocalDate.now().with(TemporalAdjusters.ofDateAdjuster(w -> {
                LocalDate result = w;
                while (!result.isEqual(day) && result.isAfter(day)) {
                    daysAlive[0]++;
                    result = result.minusDays(1);
                }
                return result;
            }));*/
            System.out.println("Żyjesz " + daysAlive + (daysAlive > 1 ? " dni" : " dzień"));
        }

        separator(reader);

        {
            System.out.println("Z6. Piątki 13-ego dnia miesiąca XX wieku:");
            reader.nextLine();
            LocalDate startDate = LocalDate.of(1900, 1, 1);
            TemporalAdjuster friday13 = TemporalAdjusters.ofDateAdjuster(w -> {
                LocalDate result = w;
                while(!(result.getDayOfWeek().getValue() == 5 && result.getDayOfMonth() == 13)) {
                    result = result.plusDays(1);
                }
                return result;
            });
            LocalDate result = startDate.with(friday13);
            while(result.getYear() < 2000) {
                System.out.println(String.format("    %s %s %s roku",
                    result.getDayOfMonth(), result.getMonth().getDisplayName(TextStyle.FULL, new Locale("pl", "PL")), result.getYear()));
                result = result.plusDays(1).with(friday13);
            }
        }

        separator(reader);

        {
            System.out.println("Z7. Sprawdzam, czy spotkanie w poniedziałek od 10ej do 13ej");
            TimeInterval intervalA = new TimeInterval(LocalTime.of(10, 0), LocalTime.of(13, 0), DayOfWeek.of(1));
            System.out.print("    nakłada się na inne spotkanie w ten sam dzień od 11ej do 12ej: ");
            TimeInterval intervalB = new TimeInterval(LocalTime.of(11, 0), LocalTime.of(12, 0), DayOfWeek.of(1));
            System.out.println(intervalA.overlapsOver(intervalB) ? "tak" : "nie");
            TimeInterval intervalC = new TimeInterval(LocalTime.of(16, 0), LocalTime.of(17, 0), DayOfWeek.of(1));
            System.out.println("    Kolejne spotkanie jest od 16ej do 17ej,");
            System.out.println("    które " + ((intervalC.overlapsOver(intervalA) || intervalC.overlapsOver(intervalB)) ? "nakłada się na poprzednie spotkania" : "nie nakłada się na żadne z poprzednich spotkań"));
        }

        separator(reader);

        {
            System.out.println("Z8. Wszystkie możliwe przesunięcia czasowe od teraz:");
            reader.nextLine();
            ZonedDateTime[] zdts = ZoneId.getAvailableZoneIds().stream()
                .map(id -> ZonedDateTime.of(
                    LocalDate.now(),
                    LocalTime.now(),
                    ZoneId.of(id)))
                .toArray(size -> new ZonedDateTime[size]);
            for (ZonedDateTime zdt : zdts) {
                System.out.println(zdt);
            }

            separator(reader);

            System.out.println("Z9. Wszystkie możliwe przesunięcia czasowe od teraz,");
            System.out.println("    których przesunięcia nie są wyrażane pełnymi godzinami:");
            reader.nextLine();
            ZonedDateTime[] zdts2 = ZoneId.getAvailableZoneIds().stream()
                .map(id -> ZonedDateTime.of(
                    LocalDate.now(),
                    LocalTime.now(),
                    ZoneId.of(id)))
                .filter(zdt -> zdt.getOffset().getTotalSeconds()%3600 != 0)
                .toArray(size -> new ZonedDateTime[size]);
            for (ZonedDateTime zdt : zdts2) {
                System.out.println(zdt);
            }
        }

        separator(reader);

        {
            System.out.println("Z10. Jeśli wylatuję z Los Angeles o godzinie 3:05pm");
            System.out.println("     i lecę przez 10 godzin i 50 minut, to do Frankfurta");
            System.out.println("     dolecę na godzinę " + calculatePeriodOffset(LocalTime.of(15,5), LocalTime.of(10,50), ZoneId.of("America/Los_Angeles"), ZoneId.of("Etc/GMT+2")) + " czasu lokalnego we Frankfurtcie.");
            // potwierdzenie:
            // https://www.timeanddate.com/time/travel.html?p1=137&p1o=20200703T150500&p2=83&p2o=20200704T015500
        }

        separator(reader);

        {
            int travelTime = calculateTravelTime(LocalTime.of(14,5), ZoneId.of("Etc/GMT+2"), LocalTime.of(16,40), ZoneId.of("America/Los_Angeles"));
            int travelHours = travelTime/60;
            int travelMinutes = travelTime - (travelHours*60);
            System.out.println("Z11. Jeśli wylatuję z Frankfurta o godzinie 14:05");
            System.out.println("     i dolatuję do Los Angeles o godzinie 16:40,");
            System.out.println(String.format("     to lot trwał %s godzin i %s minut.", travelHours, travelMinutes));
        }
    }
}