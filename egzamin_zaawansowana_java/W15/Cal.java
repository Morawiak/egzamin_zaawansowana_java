import java.time.Year;
import java.time.YearMonth;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Locale;

public class Cal {

    private static void showMonth(YearMonth month) {
        ArrayList<String[]> daysArray = new ArrayList<String[]>();
        int week = 0;
        for (int i = 1; i < month.atDay(1).getDayOfWeek().getValue(); i++) {
            if (daysArray.size() < 1)
                daysArray.add(new String[7]);
            daysArray.get(0)[i-1] = "  ";
        }
        for (int i = 1; i <= month.lengthOfMonth(); i++) {
            if (daysArray.size() < week+1)
                daysArray.add(new String[7]);
            int dayOfWeek = month.atDay(i).getDayOfWeek().getValue();
            daysArray.get(week)[dayOfWeek-1] = i < 10 ? " " + i : "" + i;
            if (dayOfWeek == 7) week++;
        }
        System.out.println(month.getMonth().getDisplayName(TextStyle.FULL_STANDALONE, new Locale("pl", "PL")));
        for (String[] weekArray : daysArray) {
            for (String dayNumber : weekArray) {
                if (dayNumber != null && !dayNumber.isEmpty())
                    System.out.print(dayNumber + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
    public static void main(String[] args) {
        if (args.length == 0)
            System.out.println("Brak argumentów. Minimum: 1");
        
        YearMonth month;
        Year year;

        if (args.length == 2) {
            try {
                year = Year.of(Integer.parseInt(args[1]));
                month = year.atMonth(Integer.parseInt(args[0]));
            } catch (Exception e) {
                System.out.println("Podano nieprawidłowy miesiąc/rok.");
                return;
            }
            showMonth(month);
        } else if (args.length == 1) {
            try {
                year = Year.of(Integer.parseInt(args[0]));
            } catch (Exception e) {
                System.out.println("Podano nieprawidłowy rok.");
                return;
            }
            
            for (int i = 1; i <= 12; i++) {
                showMonth(year.atMonth(i));
            }
        }
    }
}