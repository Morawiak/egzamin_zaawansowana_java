import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class W13 {

    private static void separator() {
        System.out.println();
        //System.out.println(String.valueOf("=").repeat(10));
        System.out.println();
    }
    public static void main(String[] args) {

        // Z1
        
	    try(Scanner reader = new Scanner(System.in)) {
            separator();

            System.out.println("Z1. Podaj liczbę:");
            
            if(reader.nextLine().matches("^\\-?\\d*[,.]?\\d+$"))
                System.out.println("Jest to liczba!");
            else
                System.out.println("Nie jest to liczba.");

            separator();

	// Z2
            
            System.out.println("Z2. Prosze podac numer domu:");
            if(reader.nextLine().matches("^\\d*[A-Z]?\\\\\\d*$|^\\d*\\\\\\d*[A-Z]?$"))
                System.out.println("Jest to numer domu.");
            else
                System.out.println("Nie jest to numer domu.");
            
            separator();

	// Z3

            System.out.println("Z3. Proszę podać nazwę miasta:");
            if(Pattern.compile("^([a-zA-Z]+\\s?)*$", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CHARACTER_CLASS).matcher(reader.nextLine()).find())
                System.out.println("Jest to nazwa miasta.");
            else
                System.out.println("Nie jest to nazwa miasta.");

            separator();
	
	// Z4

            System.out.println("Z4. Proszę podać łańcuch znaków:");
            {
                Matcher matcher = Pattern.compile("[rR]..").matcher(reader.nextLine());
                while (matcher.find()) {
                    String match = matcher.group();
                    System.out.print(match + " ");
                }
            }
            System.out.println();

            separator();


	// Z5

            System.out.println("Z5. Proszę podać łańcuch znaków:");
            {
                String line = reader.nextLine();
                Matcher matcher = Pattern.compile("\\b[cC]..\\b").matcher(line);
                while (matcher.find()) {
                    String match = matcher.group();
                    line = line.substring(0, matcher.start()) + match.toUpperCase() + line.substring(matcher.end());
                }
                System.out.println(line);
            }
            System.out.println();

            separator();

	// Z6

            System.out.println("Z6. Podaj adres URL strony (domyślnie: https://www.oracle.com/java/technologies/):");
            HrefMatch.findJPGs(reader.nextLine());

            separator();

	

	// Z8

            System.out.println("Z8. Scieżka /home/student/mojplik.txt opisuje się tak:");
            {
                String path = "/home/student/mojplik.txt";
                String[] elements = path.split("/");
                for (String string : elements) {
                    if (!string.isEmpty()) {
                        if (string.matches("[a-zA-Z0-9_-]+\\.[a-zA-Z0-9_-]+")) {
                            String[] fNameExt = string.split("\\.");
                            System.out.println("Nazwa pliku: \t\t" + fNameExt[0]);
                            System.out.println("Rozszerzenie pliku: \t" + fNameExt[1]);
                        } else System.out.println("Katalog: " + string);
                    }
                }
            }

            separator();

	// Z9

            System.out.println("Z9. W wyrażeniu \\b((\\d3)(\\d3)(\\d4))|((\\d3)(\\d4))\\b jest 7 grup.");

            separator();

	// Z10

            System.out.println("Z10. Przykład: It's 5 past 7PM");
            {
                String replExample = "It's 5 past 7PM";
                Matcher matcher = Pattern.compile("\\b[0-9]\\b").matcher(replExample);
                System.out.println("Zostało zamienione na: " + matcher.replaceAll(
                    s -> String.valueOf(Integer.parseInt(s.group()) - 3)
                ));
            }
            
        }
    }
}